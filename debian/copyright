Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: recutils
Upstream-Contact: jemarch@gnu.org
Source: <https://www.gnu.org/software/recutils/>

Files: *
Copyright: 2009-2022 Jose E. Marchesi <jemarch@gnu.org>
License: GPL-3+

Files: doc/*.texi
Copyright: 2009-2022 Jose E. Marchesi
           1994-2011 Free Software Foundation, Inc.
License: GFDL-1.3+

Files: debian/*
Copyright: 2018-2022 Sven Wick <sven.wick@gmx.de>
License: GPL-3+

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: GFDL-1.3+
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.3
 or any later version published by the Free Software Foundation;
 with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
 A copy of the license is included in the section entitled "GNU
 Free Documentation License".
 .
 On Debian systems, the complete text of the GNU Free Documentation License
 can be found in `/usr/share/common-licenses/GFDL-1.3'.
