#!/bin/bash

# Enabling the Bash builtin 'readrec'
# http://www.gnu.org/software/recutils/manual/recutils.html#Bash-Builtins
enable -f /usr/lib/recutils/bash-builtins/readrec.so readrec

while readrec
  do
    if [ $Checked = "no" ]
      then
        echo "You are been checked. $Name <${Email[0]}>"
    sleep 1
    fi
  done << RECORDS
Name: Mr. Foo
Email: foo@bar.com
Email: bar@baz.net
Checked: no

Name: Mr. Bar
Email: bar@foo.com
Telephone: 9996
Checked: no
RECORDS

