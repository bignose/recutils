#!/bin/bash

echo
recfix passwords.rec
echo
echo "You can fix un-encrypted records by decrypting and encrypting again:"
echo
echo "   $ recfix --decrypt -s mypassword passwords.rec"
echo "   $ recfix --encrypt -s mypassword passwords.rec"
echo
